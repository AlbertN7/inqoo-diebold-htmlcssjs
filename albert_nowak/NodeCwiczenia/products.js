var randomId = () => (Math.random() + 1).toString(36).substring(7)

var products = [
    {
        id: 1,
        name: "Carmona",
        image: "https://www.cocaflora.com/userdata/public/gfx/7112/Carmona-kaskada.jpg",
        price: 380.00,
    },
    {
        id: 2,
        name: "Buxus",
        image: "https://www.cocaflora.com/userdata/public/gfx/7109/Carmona.jpg",
        price: 48.00,
    },
    {
        id: 3,
        name: "Cornus",
        image: "https://www.cocaflora.com/userdata/public/gfx/7111/Carmona---przepiekna-40cm.jpg",
        price: 425.00,
    }
]


class ShopProducts {

    constructor(products, place) {
        this.products = products
        this.productsList = document.getElementById(place)
        this.renderHTMLProducts()
        this.addProduct()
        this.addProductEvent()
    }

    renderHTMLProducts(products = this.products) {

        if (this.productsList.children[0] !== undefined) this.productsList.children[0].remove()
        let newItems = document.createElement("div");

        products.map(product => {
            newItems.innerHTML +=
            `
            <div class="list-group-item" data-id="${product.id}">
                <div class="row d-flex justify-content-between ">
                    <h3>${product.name}</h3>
                    <div class="col">
                        <img class="img-fluid" src=${product.image} alt="">
                    </div>
                    <div class="col">
                        <button value="${product.id}" class="add_item btn btn-info text-nowrap">Add to Cart</button>
                        <div> ${product.price.toFixed(2)} $</div>
                    </div>
                </div>
            </div>    
            `
        })
        this.productsList.appendChild(newItems)

    }

    filtr(filters, param = 'id') {
        if (typeof (filters) !== 'object') filters = [filters]
        let filterProducts = this.products
        filters.map(filtr => {
            filterProducts = filterProducts.filter(product => { return product[param] !== filtr })
        })
        this.renderHTMLProducts(filterProducts)

    }

    addProduct() {
        let addItemToList = (e) => {
            let addItem = document.getElementById("AddProduct").elements
            this.products = [
                ...this.products,
                {
                    id: randomId(),
                    name: addItem["ProductNameInput"].value,
                    image: "https://picsum.photos/200/300",
                    price: parseFloat(addItem["ProductPriceInput"].value),
                }
            ]
            this.renderHTMLProducts()
        }
        let btnEvent = document.getElementById("AddProductBTN")
        btnEvent.onclick = addItemToList
    }


    addProductEvent() {
        fetch('http://localhost:3000/products')
        .then(response => response.json())
        .then( data => {
            
            document.getElementById('products-list').addEventListener('click', (e) => {
                let ele = e.target.closest("[data-id]")
                let status =  ele.classList.contains('active')
                document.querySelectorAll('[data-id]').forEach(ele => {ele.classList.remove('active')})
                
                if(!status) {
                    ele.classList.add('active')
                    app.productEditor.setData(data.find(pro => pro.id == parseInt(ele.getAttribute('data-id'))))
                }
    
    
            })

        })


    }
}

let shop = new ShopProducts(products, 'products-list')


