class ProductEditorView{

    constructor(selector){
        this.ele = document.querySelector(selector)
        this.form = this.ele.querySelector('form')
    }

    setData(data){
        this._data = data 
        this.form.elements['name'].value = this._data['name']
        this.form.elements['price'].value = this._data['price'].toFixed(2)

    }

    getData(){
        return this._data
    }

}

class AplicationControler{

    productEditor = new ProductEditorView('#product-editor')
    
    constructor(){

    }

}

const app = new AplicationControler()

