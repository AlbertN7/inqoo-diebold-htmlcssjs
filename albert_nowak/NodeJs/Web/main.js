const express = require('express')
const reload = require('reload')
const PORT = process.env.PORT || 8080

const products = require('./scripts/products')


const app = express()
app.set('view engine', 'pug')
app.use(express.static('views'));

app.get('/', (req, res) => { 
    res.render('main',{products: products.data})

})

// app.get('/test', (req, res) => {
//     res.render('test');
// });

reload(app).then(function (reloadReturned) {
    app.listen(PORT, () => {
        console.log(`Server -> http://localhost:${PORT}`)
    })
})