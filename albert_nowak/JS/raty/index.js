function calcLeon(credit, installment, interest_rate, type_of_instalment) {
    var allDataCredit = [
        {
            nr: 1,
            capital: credit,
            interest: credit / installment * interest_rate,
            remains: credit - credit / installment,
            instalment_amount: credit / installment + credit / installment * interest_rate,

        }
    ]
    for (let i = 1; i < installment; i++) {
        allDataCredit = [
            ...allDataCredit,
            {
                nr: allDataCredit[i - 1].nr + 1,
                capital: allDataCredit[i - 1].remains,
                interest: allDataCredit[i - 1].remains / installment * interest_rate,
                remains: allDataCredit[i - 1].remains - (credit / installment),
                instalment_amount: credit / installment + allDataCredit[i - 1].remains / installment * interest_rate,

            }
        ]
    }

    return allDataCredit
}


var displayInTable = () => {
    var installment = 12 //document.getElementById('js-installment').value
    var credit = 1000.00//parseFloat(document.getElementById('js-credit').value)
    var interest_rate = 0.10//document.getElementById('js-loan_interest_rate').value/100
    var type_of_instalment = 'down'

    var renderHTMLLeon = document.getElementById('js-table')
    renderHTMLLeon.innerHTML = ''

    var tabelLeoan = calcLeon(credit, installment, interest_rate, type_of_instalment)

    tabelLeoan.map(item => {
        var leonMap = document.createElement('tr')
        leonMap.innerHTML =
        `
        <td >${item.nr}</td>
        <td >${item.capital.toFixed(2)}</td>
        <td >${item.interest.toFixed(2)}</td>
        <td >${item.remains.toFixed(2)}</td>
        <td >${item.instalment_amount.toFixed(2)}</td>
        
        `
        renderHTMLLeon.appendChild(leonMap)
        console.log(renderHTMLLeon)
    })

    var summaryLeonData = [0, 0, 0, 0]
    for (let i = 0; i < installment; i++) {
        summaryLeonData[0] += tabelLeoan[i].capital
        summaryLeonData[1] += tabelLeoan[i].interest
        summaryLeonData[2] += tabelLeoan[i].remains
        summaryLeonData[3] += tabelLeoan[i].instalment_amount

    }

    var summaryLeon = document.createElement('tr')
    summaryLeon.innerHTML =
        `
        <td >Suma</td>
        <td >${credit}</td>
        <td >${summaryLeonData[1].toFixed(2)}</td>
        <td >${0}</td>
        <td >${summaryLeonData[3].toFixed(2)}</td>
    
        `
    renderHTMLLeon.appendChild(summaryLeon)

}

var calcButton = document.getElementById('calcButton')
calcButton.addEventListener('click', displayInTable)
