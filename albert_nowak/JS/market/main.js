
var tax = 23;
var shop_dis = 0.04;

var products = [
  {
    id: 342543,
    name: "Banana",
    price: 21.23,
    description: "Best banana ever",
    promotion: true,
    
    dis: 0.01,
    image: "https://cdn.shopify.com/s/files/1/0344/6469/products/bananabedsquare_1024x1024.png?v=1616086312",
    date_added: new Date(),
    category: ["bed"],
    get brutto() {
      return this.price * (1 + tax / 100);
    },
  },
  

  {
    id: 343232543,
    name: "Shark",
    price: 25.23,
    description: "Best shark ever",
    promotion: false,
    image: "https://m.media-amazon.com/images/I/31B-PKwNXdL._AC_.jpg",
    date_added: new Date(),
    category: ["bed"],
    get brutto() {
      return this.price * (1 + tax / 100);
    },
  },

  {
    id: 3425343,
    name: "Mira",
    price: 31.24,
    image: "https://m.media-amazon.com/images/I/61uuGOB5AFL._AC_SL1000_.jpg",
    description: "Normal bed",
    promotion: false,
    date_added: new Date(),
    category: ["bed"],
    get brutto() {
      return this.price * (1 + tax / 100);
    },
  },

  {
    id: 123341,
    price: 13.86,
    name: "Kre",
    image: "https://shop-cdn-m.mediazs.com/bilder/natural/retreat/radiator/cat/bed/5/800/80410_heizungsliege_natural_retreat_fg_9181_5.jpg",
    description: "Normal bed",
    promotion: false,
    date_added: new Date(),
    category: ["bed"],
    get brutto() {
      return this.price * (1 + tax / 100);
    },
  },

  {
    id: 12331,
    price: 53.86,
    name: "HERE",
    image: 'https://petsplusmag.com/wp-content/uploads/2019/11/MoccasinBed-front1-300x300.jpg',
    description: "Normal bed",
    promotion: true,
    dis: 0.07,
    date_added: new Date(),
    category: ["bed"],
    get brutto() {
      return this.price * (1 + tax / 100);
    },
  }
]
// debugger - Do debugowania w przeglądarce

function DataAboutProtuct(product) {
  console.log(
    `${product.name} ${product.description} ${product.promotion ? "Promo" : ""} 
   ${product.price * (1 + tax / 100)} ${product.date_added.toLocaleDateString()}`
  );
}

function PriceDisplay(product) {
  var calcPrice = null;
  product.promotion ?
    product.hasOwnProperty('dis') ?
      calcPrice = (product.price * (1 - product.dis)) * (1 + tax / 100) :
      calcPrice = (product.price * (1 - shop_dis)) * (1 + tax / 100) :
    calcPrice = (product.price) * (1 + tax / 100);
  return calcPrice
}

function renderHTML(product) {
  var item =
    `<div class="list-group-item ">
      <div class="row d-flex justify-content-between">
          <h3>${product.name}</h3>
          <div class="col">
              <p>${product.description}</p>
              <img class="img-fluid" src=${product.image} alt="">
          </div>
          <div class="col">
            <button value="${product.id}" class="add_item btn btn-info text-nowrap">Add to Cart</button>
            <div>${product.promotion ? `PROMOTION: ${Math.floor(product.dis * 100)}%` : ''}</div>
            <div> ${PriceDisplay(product).toFixed(2)} PESOS</div>
          </div>
      </div>
  </div>`


  return item
}

var swap = true

function show() {

  document.getElementById('product-list').innerHTML = ''

  if (swap) {
    for (let i = 0; i < products.length; i++) {
      document.getElementById('product-list').innerHTML += renderHTML(products[i])
    }
  }
  else {
    for (let i = products.length - 1; i >= 0; i--) {
      document.getElementById('product-list').innerHTML += renderHTML(products[i])
    }
  }

  var buttons = document.querySelectorAll('.add_item')
  buttons.forEach(button => {
    button.addEventListener('click', addToCartList)
    button.addEventListener('click', renderHTMLCart)

  })

}

var cartItemsList = []

var addToCartList = (e) => {
  var existInCart = cartItemsList.find(cartItem => cartItem.id.toString() === e.target.value)
  products.map(product => {
    product.id.toString() === e.target.value ?
      existInCart === undefined ?
      cartItemsList = [
          ...cartItemsList,
          {
            id: product.id,
            name: product.name,
            price: product.price,
            image: product.image,
            amount: 1,
            subotal: product.price,
          }
        ]
        : cartItemsList.map(cartItem => {
          cartItem.id.toString() === e.target.value ?
            (cartItem.amount += 1,
              cartItem.subotal = cartItem.price * cartItem.amount)
            : false
        })
      : false
  })
}

var increaseItemInCart = (val) => {
  cartItemsList.map(cartItem => {
    cartItem.id.toString() === val.toString() ?  
    (cartItem.amount += 1,
      cartItem.subotal = cartItem.price * cartItem.amount)
    :false
  })
  renderHTMLCart()
}

var decreaseItemInCart = (val) => {
  cartItemsList.map(cartItem => {
    cartItem.id.toString() === val.toString() ?  
    (cartItem.amount -= 1,
      cartItem.subotal = cartItem.price * cartItem.amount)
    :false
  })

  var subItem = cartItemsList.find(cartItem => cartItem.id.toString() === val.toString())
  subItem.amount < 1?
  deleteItemInCart(val)
  :false

  renderHTMLCart()
}


var deleteItemInCart = (val) => {
  var deleteItem = cartItemsList.find(cartItem => cartItem.id.toString() === val.toString())
  cartItemsList = cartItemsList.filter(cartItem =>{return cartItem !== deleteItem})
  renderHTMLCart()
}

var renderHTMLCart = () => {
  var itemInCart = ''
  cartItemsList.map(cartItem => {
    itemInCart += 
    `
    <div>
      <div>${cartItem.name}</div>
      <div>${cartItem.subotal.toFixed(2)} x ${cartItem.amount} </div>
      <div>
        <button onclick="increaseItemInCart(${cartItem.id})" type="button" class="btn btn-primary">Add</button>
        <button onclick="decreaseItemInCart(${cartItem.id})" type="button" class="btn btn-primary">Sub</button>
      </div>
      <img class="img-fluid " src=${cartItem.image} alt="">
      <button onclick="deleteItemInCart(${cartItem.id})" type="button" class=" btn-close" aria-label="Close"></button>
    </div>
    `
  })

  document.getElementById('koszyk').innerHTML = itemInCart
  document.getElementById('totalSum').innerHTML = `${totalPrice().toFixed(2)} PESOS`


}

var totalPrice = () => {
  var totalCartPrice = cartItemsList.reduce((sum,cartItem )=> {
    return sum + cartItem.subotal
  },0)
  return totalCartPriceW
} 

(() => {

  show()

  var swapMe = (e) => {
    swap = !swap
    show()
  }

  document.getElementById('swap_list').onclick = swapMe;

})()


var temp = 1;

const ok = temp => (console.log(temp))(temp)