let cartItems = []
let cartTotal = 0

function cartReset() {
  cartItems = []
  cartTotal = 0
}

function cartAddProduct(product_id) {
  const product = productsGetById(product_id)

  const item = cartItems.find(item => item.product_id === product_id)

  if (item) {
    item.amount++
    cartCalculateItem(item)
  } else {
    const amount = 1
    cartItems.push({
      // product: product,
      product_id,
      product,
      amount,
      subtotal: product.price * amount,
    })
  }
}

function cartCalculateItem(item) {
  item.subtotal = item.product.price * item.amount
}

function cartRemoveProduct(product_id) {
  const item = cartItems.find(item => item.product_id === product_id)

  if (item) {
    item.amount--
    cartCalculateItem(item)

    if (item.amount <= 0) {
      cartItems = cartItems.filter(i => i.product_id !== item.product_id)
    }
  }
}

function productsGetById(product_id) {
  return products.find(product => product.id === product_id)
}

function cartGetItems() {
  return cartItems
}

function getTotal(){
  return cartItems.reduce((sum,cartItem) =>  sum + cartItem.subtotal, 0)
} 

