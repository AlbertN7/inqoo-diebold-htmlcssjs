
class CalcView {

    constructor(selector, model) {
        this.el = document.querySelector(selector)
        if (model) {
            this.listenTo(model)
        }
    }

    listenTo(model) {
        if (!model instanceof Listenable) { throw 'Only CalcModel suported' }
        this.model = model;
        this.model.addView(this)
        this.render()
    }

    render() {
        this.el.innerHTML = /* html */`<div>
            <h1> Wynik: ${this.model.result} </h1>
        </div> `

        this.el.onclick = () => calcModel.add(1)
    }
}

class Listenable {
    listeners = []
    addView(view) {
        this.listeners.push(view)
    }
    update() {
        this.listeners.forEach(list => list.render())
    }
}

class CalcModel extends Listenable {
    result = 0;

    add(num) {
        this.result += num;
        this.update()
    }

    sub(num) {
        this.result -= num;
        this.update()
    }
}

class SpecialCalcView extends CalcView {
    render() {
        this.el.innerHTML = /* html */`<div>
                <h2 style="color:red"> Wynik: ${this.model.result} </h2>
        </div> `
        
        this.el.onclick = () => calcModel.add(1)
    }
}

// === App.js ===


// var widokElem = document.createElement('div')
// widokElem.id = 'widok'
// document.body.append(widokElem)


var calcModel = new CalcModel()

var calcView1 = new CalcView('#widok1', calcModel)

var calcView2 = new SpecialCalcView('#widok2', calcModel)

var calcView3 = new CalcView('#widok3')
    calcView3.listenTo(calcModel)


// calcModel.add(5)
// calcModel.sub(2)